    #include  <stdio.h>
    #include  <unistd.h>
    #include  <string.h>
    #include  <sys/types.h>
    #include  <sys/wait.h>
    #include  <signal.h>
    #include  <stdbool.h>
    #define   NUM_CHILD 7
    #define WITH_SIGNALS 1
    int pids[NUM_CHILD]; //table with all process ids
    int status; //status of the return signal
    int returnNr; //number of returns got
    bool isSignal; //checking if we are launching the program with signals or not
    bool isInterrupt; //checks for an interrupt
struct sigaction sa;
    void SigTerm(int sig_num) 
	{ //in case sth goes wrong - terninate the process
    signal(SIGTERM, SigTerm); //reset the signal
    printf("child[%d]: received SIGTERM signal, terminating.\n", getpid());
    isInterrupt = true;
    return 1;
    }

    void SigInt(int sig_num) 
	{ //in case sth goes wrong - terninate the process
    signal(SIGINT, SigInt); //reset the signal
    printf("child[%d]: received SIGINT signal, terminating.\n", getpid());
    isInterrupt = true;
    return 1;
    }

   void SigIntParent(int sig_num)
	{
	isInterrupt = true;
	WaitLoop();
	}






    void main(int argc, char **argv)
    {
	if(WITH_SIGNALS == 1)
	isSignal = true;
	else
	isSignal = false;

	if(isSignal)
	signal(SIGINT, SigIntParent);

	Start();
    }

    void Start()
    {
        for(int i = 0; i < NUM_CHILD; i++) //populates the table with -1s
        {
            pids[i] = -1;
        }

        RecurentCreator(NUM_CHILD);
    }


    void TerminateChildren() //send SIGTERM signal to each of the children
    {
        for(int i = 0; i< NUM_CHILD; i++)
    	{
		if(pids[i] != -1)
		{
		    kill(pids[i], SIGTERM);
		}
    	}
    }

    int ChildProcess(int i) //function happening inside child process - we check the signal, then notify about creation, wait 10 secs and notify about completion
    {

        signal(SIGTERM, SigTerm);
        printf("child[%d]: child process nr %d created.\n",getpid(), NUM_CHILD - i + 1);
        sleep(10);
        if(isInterrupt == false)
        printf("child[%d]: child process nr %d completed.\n",getpid(), NUM_CHILD - i + 1);
        return 0;
    }

    void WaitLoop() //wait loop happening inside the parent process (after the keyboard interrupt or after all children are created
    {
    bool isDone;
            while(1)
            {
            wait(&status); //we wait with the status
            if (WIFEXITED(status)){ //this checks if the status is equal to the returning of the children process
                int returned = WEXITSTATUS(status);
                if(returned == 0) //we read the status, if its 0, so everything ok, we print out the message and increment returnNr
                {
                printf("parent[%d]: exited normally with status %d\n", getpid(), returned);
                returnNr++;
                }
		if(isSignal && isInterrupt)
		{
		printf("parent[%d]: No more child processess active. Keyboard interrupt detected. Ending the execution of the parent.\n",getpid());
		TerminateChildren();
		return 0;
		}

                if(returnNr == NUM_CHILD && isInterrupt == false) //after we are done, we sleep for 1 sec (for everything to be synchronized) and then summarise the process with a message. After that we 										return 0.
                {
                    sleep(1);
                    printf("parent[%d]: No more child processess active. Ending the execution of the parent process. Received child process returns: %d", getpid(), returnNr);
                    return 0;
                }
                }
            }
    }


    void RecurentCreator(int i)
    {
    pid_t pid = fork(); //this holds the id of our fork
    pids[NUM_CHILD-i] = pid; //which is put into table, as the child processs id
    if(pid > 0)
        {
        sleep(1); //sleep for 1 sec before anything happens (for better synchronization)
        if (i == 1)
        {
            printf("parent[%d]: All children created!\n", getpid());
            WaitLoop(); //we go into the wait loop if all the children were created
        }
        else if(isInterrupt == false)
        {

            RecurentCreator(i - 1);
        }
        }
    else if(pid == 0) //if pid == 0, it means we are in the child process.
        {
            ChildProcess(i);
        }
    else //if pid < 0 it means that something is wrong - produce error message and send SIGTERM to each of the children
    {
    printf("ERROR when forking!");
    TerminateChildren();
    }
    }