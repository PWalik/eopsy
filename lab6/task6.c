#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
void copy_mmap(int fd_from, int fd_to) //function for copying the file with the use of mmap
{
    
    char *src, *dest; //two char pointers for source/destination
    size_t filesize; //size of the source file
    /*SOURCE */
    //lseek goes from the start of the file (0) to SEEK_END - of the file
    filesize = lseek(fd_from, 0, SEEK_END);
    //mapping a file in virtual memory (mapping address, length, prot type, visibility, file, offset)
    src = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd_from, 0);

    //truncates the destination file to filesize of source file
    ftruncate(fd_to, filesize);
   //mapping destination file to memory
    dest = mmap(NULL, filesize, PROT_READ | PROT_WRITE, MAP_SHARED, fd_to, 0);

    /* COPY */
    //copying form destination file from fd_from, with filesize
    memcpy(dest, src, filesize);
    //munmap frees memory both in source and destination
    munmap(src, filesize);
    munmap(dest, filesize);

    //closing both files
    close(fd_from);
    close(fd_to);

}

void copy_read_write(int fd_from, int fd_to) //function for copying the file contents using read/write
{
	size_t size; //size of the readfile
	char copyBuffer[524288]; //512kb (512 * 1024)
	size_t cutOff = sizeof(copyBuffer); //size of copyBuffer

	while((size = read(fd_from, &copyBuffer, cutOff)) > 0) //while we can still read something off the input file
	{

		if(write(fd_to, &copyBuffer, size) < 0) //write to the output file
		{
			printf("ERROR WHEN WRITING TO FILE!");
			return 1;
		}

	}
    close(fd_from); //close both files
    close(fd_to); 
    return; 
}




int main(int argc, char **argv){ //main function
    /* SOURCE */
	char Input; //input is the char of the special character (m/h)
	int from = 0, to = 0; //ints of both input and output file
	int mode = 0; //0 - readwrite, 1 - mmcpy
	while((Input = getopt(argc, argv, "mh")) != -1) //getopt takes a character after "-" and checks it 
	{
		switch(Input) //checks the input and decides if it is a help mode, mmap mode or read/write mode
		{
			case 'm':
			printf("MMAP MODE:");
			mode = 1;
			break;
			case 'h':
			printf("HELP MODE:\n acceptable inputs:\n copy [-m] <file_name> <new_file_name> \n copy [-h] \n if m is there, we go into mmap mode, if not - read write mode\n -h is help mode");
			return 0;
			break;
			default:
			return 0;
			break;
		}
	}
	  

if((from = open(argv[optind], O_RDONLY)) < 0) //open input in readonly mode
{
printf("CANNOT OPEN THE WRITE FILE, ABORTING");
return 1;
}
if((to = open(argv[optind + 1], O_RDWR | O_TRUNC | O_CREAT, 0666)) < 0) //open output file in readwrite mode, we will use truncating to achieve the copy, and if the file doesnt exist - create it
{
printf("CANNOT OPEN THE WRITE FILE, ABORTING");
return 1;
} 
//rdwrite/created by truncating a ready file/create files you cannot get to
if(mode == 1)
{
	copy_mmap(from, to); //go to copy mmap
}
else 
{
	printf("READ_WRITE MODE");
	copy_read_write(from, to); //go to readwrite
}
return 0;
}
