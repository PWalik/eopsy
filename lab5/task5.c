#include <unistd.h> 
#include <semaphore.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#define N 5  
#define MEALS 3
#define WAITTIME 2
#define EATTIME 3
int philID;
int semID;

void grab_forks(int leftForkID)
{
	int rightForkID = leftForkID + 1;
	if(rightForkID > N -1)
	{
		rightForkID = 0;
	}
	printf("Philosopher %d tries to pick up forks %d and %d \n", leftForkID, leftForkID, rightForkID);

	struct sembuf semaphorFork[2] = 
	{
	{leftForkID, -1, 0},
	{rightForkID, -1, 0}
	};
	//semaphorID, semaphorBuffer, HowManySemaphores
	semop(semID, semaphorFork, 2);
}

void put_away_forks(int leftForkID)
{
	int rightForkID = leftForkID + 1;
	if(rightForkID > N -1)
	{
		rightForkID = 0;
	}
	printf("Philosopher %d puts away forks %d and %d \n", leftForkID, leftForkID, rightForkID);

	struct sembuf semaphorFork[2] = 
	{
	{leftForkID, 1, 0},
	{rightForkID, 1, 0}
	};
	semop(semID, semaphorFork, 2);
}

void eat(int philID, int mealNumber)
{
	grab_forks(philID);
	printf("Philosopher %d is eating\n", philID);
	sleep(EATTIME);
	put_away_forks(philID);
}

void think(philID)
{
	printf("Philosopher %d is thinking \n", philID);
	sleep(WAITTIME);
}

void PhilBehavior(int philosopherID)
{
	int meals = MEALS;
	int hunger = 0;
	printf("Philosopher %d joins and sits at the table\n", philosopherID);
	while(meals > 0)
	{
		if(hunger == 1)
		{
			eat(philosopherID, --meals);
			hunger = 0;
		}
		else
		{
			think(philosopherID);
			hunger = 1;
		}
	}
	printf("Philosopher %d has eaten and leaves the table\n", philosopherID);
}

int main() 
{
	printf("Start of the program\n");
	philID = 0;
	int pId;
	int waitStatus;
	//0644 are the passing of permissions
	//semaphores are used to assign philosophers to forks
	semID = semget(111153, 5, IPC_CREAT | 0644);

	for(int i = 0; i< N; i++) 
	{
		//semctl is used for control of semaphores
		//SETVAL is used for setting values 
		//(1 means that fork is free and available)
		semctl(semID, i, SETVAL, 1);
	}

	for(int i = 0; i < N; i++)
	{
		pId = fork();
		if(pId == 0)
			{	
			philID = i;
			PhilBehavior(philID);
			return 0;		
			}
		else if(pId < 0)
			{
			kill(-2, SIGTERM);
			printf("Cannot create a process - critical ERROR\n");
			exit(1);
			}
		}
	while(1) 
	{
		pId = wait(&waitStatus);
		if(pId < 0)
			break;
	}
	//RMID removes the set semaphore
	if(semctl(semID, 0, IPC_RMID,1)<0)
		return 0;

	printf("End of the program\n");
	return 0;
}
