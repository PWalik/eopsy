#include <stdio.h> 
#include <pthread.h>
#include<stdlib.h>

#define N 5  
#define MEALS 3
#define WAITTIME 2
#define EATTIME 3
#define HUNGRY 2
#define EATING 3
#define THINKING 1
#define LEFT	( i + N - 1 ) % N
#define RIGHT	( i + 1 ) % N

pthread_mutex_t m; //the internal mutex
pthread_mutex_t s[N]; //array of all the philosopher mutexes initialized in main()
pthread_t p[N]; //list of thread ids of philosophers created in main
int state[N]; //current philosopher status (hungry, eating, thinking)

void grab_forks( int i ) //function for philosopher i to grab fork
{
	pthread_mutex_lock( &m ); //we lock the internal mutex (to restrict the other threads from running
		state[i] = HUNGRY;
		test( i );
	pthread_mutex_unlock( &m );//we unlock the internal mutex after we're done
	pthread_mutex_lock( &s[i] ); //we lock the mutex id of the philosopher
}

void
put_away_forks( int i ) //function for philosopher i to put away forks
{
	pthread_mutex_lock( &m );//we lock the internal mutex (to restrict the other threads from running
		state[i] = THINKING;
		test( LEFT );
		test( RIGHT );
	pthread_mutex_unlock( &m ); //we unlock the internal mutex after we're done
}

void test( int i )
{
	if( state[i] == HUNGRY
		&& state[RIGHT] != EATING
		&& state[LEFT] != EATING ) //if a phil is hungry and 							both phils to his left and right are not eating, that means that we can eat
	{
		state[i] = EATING;
		pthread_mutex_unlock( &s[i] ); //unlock the phil mutex
	}
}
void eat(int philID, int mealNumber) //function for eating by phil
{
	grab_forks(philID); //phil tries to grab forks, then waits and puts them away
	printf("Philosopher %d is eating\n", philID);
	sleep(EATTIME);
	put_away_forks(philID);
}

void think(philID) //functions for thinking philosopher - we wait for WAITTIME and change his status to THINKING
{
	printf("Philosopher %d is thinking \n", philID);
	state[philID] = THINKING;
	sleep(WAITTIME);
}

void* PhilBehavior(int philosopherID) //main function for the Phil behavior
{
	int meals = MEALS; //number of meals phil has to eat to leave
	int hunger = 0; //hunger check - 0 not hungry, 1 hungry
	printf("Philosopher %d joins and sits at the table\n", philosopherID);
	while(meals > 0) //as long as we havent eaten fully, we either try to eat or think
	{
		if(hunger == 1)
		{
			eat(philosopherID, --meals);
			hunger = 0;
		}
		else
		{
			think(philosopherID);
			hunger = 1;
		}
	} //once it finishes, the phil leaves the table
	printf("Philosopher %d has eaten and leaves the table\n", philosopherID);
}

int main() 
{
	printf("Start of the program\n");
	pthread_mutex_init(&m, NULL); //initializing m
	pthread_mutex_unlock(&m); //unlocking m by default
	void *status;
		for (int i = 0 ; i < N ; i++) 
	{ 
		state[i] = THINKING;//all philosophers are thinking
		pthread_mutex_init(&s[i], NULL);//at first all mutex 							are locked
		pthread_mutex_lock(&s[i]);
	}
	
	for(int i = 0; i < N; i++)
	{
		//create new thread for each of the philosophers
		//thread, attribute, start_routine, argument
	if(pthread_create( &p[i], NULL, (void *)PhilBehavior, (void*)i ) != 0) 
	    { 
	        perror("pthread_create() failure."); 
	        exit(1); 
	    } 
		}

	//join method makes one thread wait for the completion of other ones.
    for (int i = 0; i < N; i++) 
    { 
	//if join doesnt work, create an error
	//thread, return val
        if(!pthread_join(p[i], &status )==0) 
        { 
            perror("thr_join() failure."); 
            exit(1); 
        } 
    }  

	for (int i = 0 ; i < N ; i++)
{
		pthread_mutex_destroy(&s[i]);//at the end of the program we destroy the mutexes
}
	printf("End of the program\n");
	return 0;
}