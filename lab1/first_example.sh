#!/bin/bash
echo "Starting state - test1 and TEST2 in home, test3 in /testfold"
./first.sh -r -l TEST2
echo "Lowercased TEST2 in the folder with recursion, making it test2"
ls -R
./first.sh -u test1 test2 test3
echo "Uppercased all 3 files, but without recursion (so it only changes test1 and test2 to TEST1 TEST2"
ls -R
./first.sh -r -u test3
echo "uppercasing the test3 using recursion"
ls -R
./first.sh -r s/3/1/g TEST3 #sed function - s/ for substitute, /g for global- changes 							all occurences of string, not just 							first one
echo "SED function - changing TEST3 to TEST1 using recursion"
ls -R
./first.sh -h
