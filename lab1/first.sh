#!/bin/bash
param="$1" #first parameter (can be changed after, depending on the recursion state)
number=2 #current number at which the filenames start
recursed=0 #check if recursion is enabled or not
cdir=$PWD #starting directory of the script
change() #function for changing the name (uppercasing, lowercasing or sed)
{
if [ -f "$1" ] ; then #checking if file exists
if [ $2 = "-u" ] ; then	#if parameter -u then uppercasing with mv(^^)
	echo "Changed $1 to ${1^^}"
	mv -- "$1" "${1^^}" #-- marks the end of parameter list
elif [ $2 = "-l" ] ; then #if parameter -l then lowercasing with mv(,,)
	echo "Changed $1 to ${1,,}"
	mv -- "$1" "${1,,}"
else
	echo "SED "$2" executed on $1"
	newname=$(echo "$1" | sed -e "$2")	#sed executed on the string, then 							name swapped
	mv -- "$1" "$newname"
fi
fi

}

recurse() #function for recursion
{
dir=$(find . -name "$name") #finds the file name among all the subfolders and gets 					the directory of it
dir=$(echo $dir | sed 's/'$name'.*//g') #chops the last part of dir (one with 						filename)
echo "$dir"
cd $dir #goes to the directory of the file
change $1 $2	#does the change on the file
cd $cdir	#goes back to the start directory
}

if [ "$param" == "-r" ] ; then #if the parameter is -r, so recursion, ++ the number
	param="$2"
	recursed="1"
	number=$((number+1))

fi

case "$param" in #case for the parameter (if help, show help, if recursive go 			recursive, if not, just call change
	"-h") echo "HELP MODE"
	echo "use parameters to operate the script:"
	echo "-r : recursive mode, go through each of the folders looking for files"
	echo "-u : uppercase the file name"
	echo "-l : lowercase the file name"
	echo "-h : help mode"
	echo "instead of -u/-l you can input a custom SED pattern."
	;;
	*)
	for name in ${@:number} #iterates over all the filenames
do
	if [ "$recursed" -eq "1" ] ; then #checks if recursed (so if parameter is -r)
	recurse $name $param #if it is, do recursion
	fi
	change $name $param #if not, do change
done
	;;
esac


